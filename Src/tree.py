import random

from .dataset import Dataset
from .node import Node
from .utils import c

import math
import numpy as np

class Tree :

    def __init__(self, f, ids):
        self.forest = f
        self.dataSet = f.dataSet
        self.ids = ids
        self.isolationNodes = []
        self.depthLimitNodes = []
        self.denseNodes = []
        self.root = self.build(self.ids, 0)
        
    
        
    def __str__(self):
        ret="TREE | IDS : "+str(self.ids)+"\n"
        
        return ret + self.recStr(self.root,0)


    def recStr(self, n,i):
        ret = ""
        if n is not None:
            ret="\t"*i+str(n)+"\n"+self.recStr(n.leftNode,i+1)+"\n"+self.recStr(n.rightNode,i+1)
        return ret
           

    def build(self, idsR, currDepth:int, margins=None):

        """
        Builds a tree
        @param idsR: indices of the data points in the sample
        @param currDepth: current depth of the tree
        @param margins: array containing the width of the margins on each attribute
        @return: a node (internal or leaf node) of the tree
        """
        # Build a CADI tree
        if self.forest.METHOD == "cadi":
            nbPtsNearLeft=0 # Number of points in the left margin
            nbPtsNearRight=0 # Number of points in the right margin
            
            tested = []
            excluded = None

            # Half of the width of the margin (in %)
            proxAreaRange = self.forest.ALPHA / 2
            
            attributes = self.dataSet.getAttributes()
             
            if margins is None:
                margins = [None for _ in range(len(attributes))]
            
            
            if len(idsR) <= 1:
                self.isolationNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            elif currDepth >= self.forest.MAXHEIGHT:
                self.depthLimitNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            else:
                
                a = random.randint(0, len(attributes)-1)
                
                mini = math.inf
                maxi = (-math.inf)
                
                for ix in idsR: # Getting the max and min values of the points in ix
                    data = self.dataSet.getData(ix)
                    if data[a] > maxi: maxi = data[a] 
                    if data[a] < mini: mini = data[a]
                    
                if margins[a] is None:
                    marginWidth = (maxi-mini) * proxAreaRange
                    margins[a] = marginWidth
                else:
                    marginWidth = margins[a]
                    
                v = random.uniform(mini, maxi)
                
                proxLeftBorder = max(mini, v - marginWidth)
                proxRightBorder = min(maxi, v + marginWidth)

                idsLeft = []
                idsRight = []
            
                
               # Assigning the data points to the left and right parts of the separation
                for ix in idsR:
                    if self.dataSet.getData(ix)[a] <= v:
                        idsLeft.append(ix)

                        if self.dataSet.getData(ix)[a] >= proxLeftBorder:
                            nbPtsNearLeft = nbPtsNearLeft +1

                    else:
                        idsRight.append(ix)

                        if self.dataSet.getData(ix)[a] <= proxRightBorder:
                            nbPtsNearRight = nbPtsNearRight +1
                            
                
                # Computing the ratio of the density of data points in the margin (left + right) and the density of data points in the node
                
                localDensity = (nbPtsNearLeft + nbPtsNearRight)

                areaDensity = len(idsR) 


                # Testing if the separation needs to be kept
                # Density threshold
                thresh = (self.forest.ALPHA * areaDensity)

                
                while(localDensity > int(thresh)):
  
                    if excluded is None:
                        excluded = [None for _ in range(len(attributes))]
                        if excluded[a] is None:
                            excluded[a] = []
                        excluded[a].append(proxLeftBorder)    
                    else:
                        if excluded[a] is None:
                            excluded[a] = []
                        excluded[a].append(proxLeftBorder)
                    
                    values = excluded[a]
                    choices = [mini] + sorted(values) + [maxi]
                        
                    i = 0
                    while((i < (len(choices) - 1)) and ((choices[i] + 2 * marginWidth) >= choices[i+1])):
                        i = i+1
                    if (i == len(choices) - 1 ):
                        tested.append(a)
                        untestedAttributes = [j for j in range(len(attributes)) if j not in tested]
                        
                        if (len(tested) == len(attributes)):
                            
                            self.denseNodes.append(idsR)
                            return Node(idsR, currDepth, None, None, None, None)
                        else:
                            
                            a = random.choice(untestedAttributes)
                            
                            mini = math.inf
                            maxi = (-math.inf)
                            
                            for ix in idsR: # Getting the max and min values of the points in ix
                                data = self.dataSet.getData(ix)
                                if data[a] > maxi: maxi = data[a] 
                                if data[a] < mini: mini = data[a]
                                
                            if margins[a] is None:
                                marginWidth = (maxi-mini) * proxAreaRange
                                margins[a] = marginWidth
                            else:
                                marginWidth = margins[a]
                                
                            v = random.uniform(mini, maxi)
                    else:
                        v = random.uniform(choices[i] + 2 * marginWidth, choices[i+1])
                
                    proxLeftBorder = max(mini, v - marginWidth)
                    proxRightBorder = min(maxi, v + marginWidth)
        
                    idsLeft = []
                    idsRight = []
                    nbPtsNearLeft=0 # Number of points in the left margin
                    nbPtsNearRight=0 # Number of points in the right margin
                    
                    # Assigning the data points to the left and right parts of the separation
                    for ix in idsR:
                        if self.dataSet.getData(ix)[a] <= v:
                            idsLeft.append(ix)
        
                            if self.dataSet.getData(ix)[a] >= proxLeftBorder:
                                nbPtsNearLeft = nbPtsNearLeft +1
        
                        else:
                            idsRight.append(ix)
        
                            if self.dataSet.getData(ix)[a] <= proxRightBorder:
                                nbPtsNearRight = nbPtsNearRight +1
                                
                    localDensity = (nbPtsNearLeft + nbPtsNearRight)
                        
                if (localDensity <= int(thresh)):
                    return Node(idsR, currDepth, a, v, self.build(np.array(idsLeft), currDepth + 1, margins=margins), self.build(np.array(idsRight), currDepth + 1, margins=margins))
        
        
        # Build an IF tree
        else:
            attributes = self.dataSet.getAttributes()
            a = random.randint(0, len(attributes)-1)
            
            if len(idsR) <= 1:
                self.isolationNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            elif currDepth >= self.forest.MAXHEIGHT:
                self.depthLimitNodes.append(idsR)
                return Node(idsR, currDepth, None, None, None,None)
            else:
                
                mini = math.inf
                maxi = (-math.inf)
                
                for ix in idsR: # Getting the max and min values of the points in ix
                    data = self.dataSet.getData(ix)
                    if data[a] > maxi: maxi = data[a] 
                    if data[a] < mini: mini = data[a]
                    
                v = random.uniform(mini, maxi)
                idsLeft = []
                idsRight = []         
    
                # Assigning the data points to the left and right parts of the separation
                for ix in idsR:
                    if self.dataSet.getData(ix)[a] <= v:
                        idsLeft.append(ix)
    
                    else:
                        idsRight.append(ix)
    
    
                return Node(idsR, currDepth, a, v, self.build(np.array(idsLeft), currDepth + 1), self.build(np.array(idsRight), currDepth + 1))           
  
    
    def pathLength(self, point, node, e):
        """
        Computes the path's length of a data point in the tree from Node node.
        To compute the path's length in the entire tree, set node to the root of the tree.
        Parameters
        ----------
        point : 1-D array of floats
            coordinates of the data point.
        node : Node
            the Node from which the search starts.
        e : integer
            current path length.
        Returns
        -------
        float
            length of the path of the data point.
        """
        if((node.leftNode is not None) or (node.rightNode is not None)):
            # (If it is an internal node)
            if point[node.sepAtt] < node.sepVal:
                return self.pathLength(point, node.leftNode, e+1)
            else:
                return self.pathLength(point, node.rightNode, e+1)
        else:
            # (If it is an external node)
            return e + c(len(node.ids))
        

        
    def getNode(self, point, node):
    
        
        while((node.leftNode is not None) and (node.rightNode is not None)):
            if point[node.sepAtt] < node.sepVal:
                return self.getNode(point, node.leftNode)
            else:
                return self.getNode(point, node.rightNode)
        return node
    
    
    def is_dn_leaf(self, leaf):
        """
        Finds out if the specified leaf is a leaf of the tree
        @param leaf: array containing the indices of the points in the leaf
        @return: boolean
        """
        for dn_leaf in self.denseNodes:
            if np.array_equal(dn_leaf, leaf):
                return True
        return False
        
    
    def get_ancestor_attributescount(self, idx_anomaly, idx_leaf_point):
    
        attributes_count = {} # Index : attribut, valeur : nombre de fois où il a été utilisé

        point_anomaly = self.dataSet.getData(idx_anomaly)
        point_leaf = self.dataSet.getData(idx_leaf_point)
        ancestor = None
        current_node = self.root

        for att in range(len(self.forest.dataSet.getAttributes())):
            attributes_count[att] = 0


        while current_node.leftNode is not None:
            ancestor = current_node
            attributes_count[ancestor.sepAtt] += 1

            if (point_anomaly[current_node.sepAtt]<= current_node.sepVal and point_leaf[current_node.sepAtt]<= current_node.sepVal):
                current_node = current_node.leftNode
            elif (point_anomaly[current_node.sepAtt]> current_node.sepVal and point_leaf[current_node.sepAtt]> current_node.sepVal):
                current_node = current_node.rightNode
            else: # The split separes the anomaly to explain and the points from the leaf
                attributes_count[ancestor.sepAtt] -= 1
                return ancestor, attributes_count, ancestor.sepAtt
        ancestor = current_node
        return ancestor, attributes_count, None
    
    
        
    def computeScoreModified(self, point):
        
        node = self.getNode(point, self.root)
        
        return 1- ((len(node.ids) - 1)/len(self.root.ids))
        
        # neighboursNumber = len(node.ids)
        
        # score = (1 - (neighboursNumber * (2 ** (node.depth - 1)))/len(self.root.ids))
        # if score < 0:
        #     return 0
        # else:
        #     return score




        