import numpy as np

def c(n):
    """
    Computes the adjustment c(Size) : c(i) = ln(i) + Euler's constant
    
    Parameters
    ----------
    n : integer
        size of the sample.

    Returns
    -------
    float
        adjustment value.

    """
    if n > 2:
        return 2.0 * (np.log(n-1) + np.euler_gamma) - 2.0 * (n-1.0) / n
    elif n == 2:
        return 1.0
    else:
        return 0.0
    
    
def compute_similarity(l1, l2):
    return len(np.intersect1d(l1, l2))/len(np.unique(np.concatenate((l1, l2))))


def test_inclusion(l1, l2):
    common = 0
    if len(l1) <= len(l2):
        for i in l1:
            if i in l2:
                common += 1
        if common == len(l1):
            return True
        else:
            return False 
    else:
        return False
    
        
def get_idx_in_array(arr1, arr2):
    for i in range(len(arr1)):
        if np.array_equal(arr1[i], arr2):
            return i
    return -1

# Graph utils

def get_neighbors(id_leaf, simi_mat, simi_thresh):
    neighbors = []
    for j in range(len(simi_mat)):
        if simi_mat[id_leaf][j] > simi_thresh:
            neighbors.append(j)
    return neighbors

def dfs(visited, node, component, similarity_matrix, similarity_thresh):
    visited[node] = True
    component.append(node)
    for neighbor in get_neighbors(node, similarity_matrix, similarity_thresh):
        if not visited[neighbor]:
            dfs(visited, neighbor, component, similarity_matrix, similarity_thresh)

def connected_components(n_nodes, similarity_matrix, similarity_thresh):
    visited = [False] * n_nodes
    components = []
    for node in range(n_nodes):
        if any(simie > similarity_thresh for simie in similarity_matrix[node]) and (not visited[node]):
            component = []
            dfs(visited, node, component, similarity_matrix, similarity_thresh)
            components.append(component)
    return components

## Fonction pour retrouver les noeuds isolés du graphe. Renvoie une liste contenant les index (nouveaux, après pré-traitement) des noeuds isolés

def get_isolated_nodes(simi_mat):
    l = len(simi_mat)
    isolated_nodes = []
    
    for key, val in enumerate(simi_mat):
        i = 0
        while (i<l) and (val[i]==0):
            i += 1
        if i==l:
            isolated_nodes.append(key)
    return isolated_nodes





