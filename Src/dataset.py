#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 21:08:30 2020

@author: SHAMAN
"""

import numpy as np

class Dataset:
    
    # Modifier la fonction pour gérer directement le fichier csv (pas besoin de header etc). Gérer le cas où les noms de colonnes sont présents (stocker les noms de colonne 
    # dans ce cas, lors de l'affichage des explications), où la dernière colonne c'est le label. 
    def __init__(self, filepath, columns=None, labels=True):
        """
        Load a csv dataset 
        """

        data = np.loadtxt(filepath, delimiter=",", converters=lambda s: float(s.strip() or 0), skiprows=0) # is a np.ndarray

        if columns is None:
            self.attributes = [str(i) for i in range(data.shape[1])]
        else:
            self.attributes = columns
        if labels:
            self.data = data[:, :-1]
            self.attributes = self.attributes[:-1]
            self.hasLabel = True
        else:
            self.data = data
            self.hasLabel = False
        self.nbPoints = len(self.data)



    def getNbPoints(self):
        return self.nbPoints


    def getData(self, i=None):
        if i is not None:
            return self.data[i]
        else:
            return self.data


    def getClass(self,i):
        if self.hasLabel:
            return self.data[i][len(self.getAttributes())]
        else:
            print("Provide labels.") # TODO renvoyer un exception. Dans la fonction d'après aussi

    
    def getAnomaliesIndices(self):
        if self.hasLabel:
            return [i for i in range(self.getNbPoints()) if (self.getClass(i) == 1)]
        else:
            print("Provide labels.")


    def getAttributes(self):
        return self.attributes

        
    def binClassesVector(self):
        res = np.zeros(self.nbPoints)
        j=0
        for i in range(self.nbPoints):
            pt = self.data[i]
            res[j] = int(pt[len(pt)-1])
            j=j+1
        return res


