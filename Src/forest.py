import random
import math
import numpy as np

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

from .dataset import Dataset
from .tree import Tree
from .utils import *

class Forest:

    def __init__(self, d: Dataset, nbT : int = 100, method="normal", psi=256, maxHeight=math.ceil(math.log(256,2)), alpha=0.05, tau=0):
        """

        @param d: the dataset object
        @param nbT: the number of trees
        @param method: the method to use : "normal" for IF or "cadi" for CADI
        @param psi: the sample size of each tree
        @param maxHeight: the depth limit of each tree
        @param alpha: the margin width around each split, in percentage
        @param tau: the weight edges threshold, in percentage
        """
        random.seed()
        self.dataSet = d
        self.trees = []
        self.scores = []
        self.anomalies = []
        self.connected_components = []
        self.clusters_affectations = []
        self.local_affectations = {}
        self.explanations = []
        # Hyperparameters
        self.NBTREES = nbT
        self.METHOD = method
        self.MAXSIZESS = psi
        self.MAXHEIGHT = maxHeight
        self.ALPHA = alpha
        self.TAU = tau
        
        
    def __str__(self):
        ret = "FOREST | NBTREES : "+str(self.NBTREES)+ " MAXSIZE : "+str(self.MAXSIZESS)+" ALPHA : "+str(self.ALPHA)
        for i in range(len(self.trees)):
            ret += "\n"+str(self.trees[i])
        return ret

        
    def build(self):
        """
        Builds the isolation forest
        """
        # Permutation of the ids
        rng = np.random.default_rng()
        nbPoints = self.dataSet.getNbPoints()
        sampleSize = min(self.MAXSIZESS, nbPoints)
        for i in range(self.NBTREES):
            sampleIds = rng.choice(range(self.dataSet.getNbPoints()), sampleSize, replace=False)
            self.trees.append(Tree(self, sampleIds))
 
    
    

    def computeScore(self, x):
        """
        Computes the anomaly score of an instance. Two versions of the scores: IF or CADI
        (determined by the type of forest built)
        @param x: the instance
        @return: the anomaly score of the instance
        """
        
        if self.METHOD == "cadi":
            score = 0.0
            for i in range(self.NBTREES):
                score = score + self.trees[i].computeScoreModified(x)
            return (score / self.NBTREES)
            
        else:
            hx = 0.0
            for i in range(self.NBTREES):
                hx = hx + self.trees[i].pathLength(x, self.trees[i].root, 0)
            hx = hx / self.NBTREES
            nbPoints = self.dataSet.getNbPoints()
            sampleSize = min(self.MAXSIZESS, nbPoints)
            return 2 ** (-hx / c(sampleSize))
    

    def computeScores(self):
        """
        Computes the anomaly scores of the points in the dataset
        @return: anomaly scores
        """
        self.scores = np.apply_along_axis(self.computeScore, 1, self.dataSet.getData())
        return self.scores
    
    
    def anomalyDetection(self, binary=False, contamination_rate=None):
        """
        Performs anomaly detection on the dataset using the built forest
        @param binary:
        @param contamination_rate: contamination rate, between 0 (exclusive) and 1 (exclusive). Typically > 0.1
        @return: computed anomaly scores if binary=False, anomalies otherwise
        """
        self.scores = np.apply_along_axis(self.computeScore, 1, self.dataSet.getData())
        if binary:
            if contamination_rate is None or contamination_rate < 0.0:
                # todo throw an exception
                print("Binary labels but contamination rate not set or invalid")
            else:
                self.anomalies = np.argsort(self.scores)[-int(contamination_rate*len(self.scores)):]
            return self.anomalies
        else:
            return self.scores


    def get_tree_dnleaf(self, leaf):
        """
        Returns the tree to which the leaf belongs
        @param leaf: array containing the indices of the instances in the leaf
        @return: the index of the tree to which the leaf belongs
        """
        for t in range(self.NBTREES):
            if self.trees[t].is_dn_leaf(leaf):
                return t
    
    
    def clustering(self):
        """
        Performs clustering
        @return: None
        """

        #todo if len(self.anomalies) == 0:
        #     print("Identify the anomalies first with the method anomalyDetection()")
        
        #else:
        
        # Extracting DN leaves
        
        dn_leaves = []
        trees_leaves = {}
        trees_dn_preprocessed = {}
        to_drop = []

        idx_leaf = 0
        
        for t in range(self.NBTREES):
            trees_leaves[t] = []
            for n in self.trees[t].denseNodes: 
                dn_leaves.append(n)
                trees_leaves[t].append(n)

        # todo if dn_leaves is empty, stop there and inform the user

        # Removing leaves fully included in others
        
        for i in range(len(dn_leaves)):
            for j in range(i):
                if test_inclusion(dn_leaves[i], dn_leaves[j]):
                    to_drop.append(i)
                elif test_inclusion(dn_leaves[j], dn_leaves[i]):
                    to_drop.append(j)
        
        to_drop = np.unique(to_drop)
        dn_leaves_preprocessed = []
        
        i = 0
        for t in range(self.NBTREES):
            trees_dn_preprocessed[t] = []
            leaves = trees_leaves[t]
            for leaf in trees_leaves[t]:
                if i not in to_drop:
                    dn_leaves_preprocessed.append(leaf)
                    trees_dn_preprocessed[t].append(leaf)
                i = i+1

        c = len(dn_leaves_preprocessed)
        similarity_matrix = np.zeros((c,c))
        for i in range(c):
            for j in range(i):
                similarity_matrix[i][j] = compute_similarity(dn_leaves_preprocessed[i], dn_leaves_preprocessed[j])
                similarity_matrix[j][i] = similarity_matrix[i][j]

        # Keeping only the edges with a weight greater than the tau-th percentile: computing the weight threshold
        similarity_thresh = 0
        if self.TAU > 0:
            similarity_matrix_flat = similarity_matrix.flatten()
            similarity_matrix_flat_nonzeros = similarity_matrix_flat[similarity_matrix_flat!=0]
            similarity_thresh = np.percentile(similarity_matrix_flat_nonzeros, self.TAU)
        
        # Extracting the connected components
        components = connected_components(c, similarity_matrix, similarity_thresh)
    
    
    
        # Clustering from the connected components
        print("Clustering from connected components.....")
        idx_cluster = 0
        clustering_leaves = {}
        clustering_points = {}

        for component in components:
            idx_cluster += 1
            for idx_leaf in component:

                clustering_leaves[idx_leaf] = idx_cluster
                for idx_point in dn_leaves_preprocessed[idx_leaf]:
                    if idx_point not in clustering_points.keys():
                        clustering_points[idx_point] = []
                    clustering_points[idx_point].append(idx_cluster)

        ### Cas des noeuds isolés : Nouveaux clusters ou -anomalies-
        for new_idx_iso in get_isolated_nodes(similarity_matrix):
            idx_cluster += 1
            #idx_cluster = - 1
            clustering_leaves[new_idx_iso] = idx_cluster

            for idx_point in dn_leaves_preprocessed[new_idx_iso]:
                if idx_point not in clustering_points.keys():
                    clustering_points[idx_point] = []
                clustering_points[idx_point].append(idx_cluster)


        ## Partitionnement en tenant compte des anomalies et des points hors échantillons: 
        print("Partition with anomalies and out-of-sample points...")
        for i in range(self.dataSet.getNbPoints()):
            if i in self.anomalies:
                # Gestion des anomalies
                clustering_points[i] = [-1]
            elif i not in clustering_points.keys():
                # Propagation des points hors échantillons
                clust_i = []
                for t in range(self.NBTREES):
                    tree = self.trees[t]
                    if i not in tree.ids:
                        node_i = tree.getNode(self.dataSet.getData(i), tree.root)
                        id_leaf = get_idx_in_array(trees_dn_preprocessed[t], node_i.ids)
                        if id_leaf != - 1:
                            clust_i.append(clustering_leaves[id_leaf])
                    clustering_points[i] = clust_i

        ## Final partition
        ##########################################
        print("Final Partition....")
        final_labels_f = []
        for i in range(self.dataSet.getNbPoints()):
            candidates = clustering_points[i]
            if len(candidates) > 1:
                final_labels_f.append(np.bincount(candidates).argmax())
            elif len(candidates) == 1:
                final_labels_f.append(candidates[0])
            else:
                final_labels_f.append(-1)
        
        self.clusters_affectations = final_labels_f
        self.connected_components = [[dn_leaves_preprocessed[k] for k in component] for component in components]
        
    
            
    def explain_anomalies(self):
        """
        Computes explanations for each identified anomaly
        @return: None
        """
        
        explanations = {}
        
        for id_anomaly in self.anomalies:
            
            local_affectation = []

            total_count = {} # index entier qui est celui de la composante connexe, 
                         # valeur qui est un dictionnaire représentant le compte total pour chaque attribut
            normalized_count = {} # valeur dictionnaire représentant le compte normalisé pour chaque attribut

            discr_count = {}
            normalized_discr_count = {}

            simi = np.zeros(len(self.connected_components))
            max_sim = 0
            
            i = 0
            for component in self.connected_components:
                # todo if i in self.clusters_affectations, to remove the connected components that have disappeared
                
                total_count[i] = {}
                normalized_count[i] = {}
                discr_count[i] = {}
                normalized_discr_count[i] = {}
                
                for att in range(len(self.dataSet.getAttributes())):
                    total_count[i][att] = 0
                    discr_count[i][att] = 0
                    
                
                for j in range(len(component)):
                    ances, countt, disc_attr = self.trees[self.get_tree_dnleaf(component[j])].get_ancestor_attributescount(id_anomaly, component[j][0])
                    simi[i] += ances.depth
                    if disc_attr is not None:
                        discr_count[i][disc_attr] += 1

                    for attr in countt.keys():
                        total_count[i][attr] += countt[attr]
                
                for attr in total_count[i].keys():
                    normalized_count[i][self.dataSet.getAttributes()[attr]] = total_count[i][attr] / len(component)
                    normalized_discr_count[i][self.dataSet.getAttributes()[attr]] = discr_count[i][attr] / len(component)
                simi[i] /= len(component)
                
                if int(simi[i]) >= max_sim:
                        max_sim = int(simi[i])
                i += 1

            for i in range(len(self.connected_components)):
                # todo if i in self.clusters_affectations, to remove the connected components that have disappeared
                if int(simi[i]) == max_sim: # Partie entière
                    local_affectation.append(i)
            

                    
            
            explanations[id_anomaly] = {"local_affectation" : local_affectation, 
                                        "common_attributes" : {c+1 : normalized_count[c] for c in local_affectation},
                                        "discriminating_attributes" : {c+1 : normalized_discr_count[c] for c in local_affectation}
                                        }

        self.explanations = explanations

    
    def computeAUC(self,scores):
        """
        Computes the AUC
        @param scores: array containing the anomaly scores of each instance in the dataset
        @return:
        """
        binData = self.dataSet.binClassesVector()
        ruc = roc_auc_score(binData, scores)
        ns_fpr, ns_tpr, _ = roc_curve(binData, scores)
        return ruc,ns_fpr,ns_tpr
        

class ForestParametersNotSet(Exception):
    """Raised when the input value is too large"""
    pass