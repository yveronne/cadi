# CADI

***

This is the code source of the unified anomaly detection and explanation algorithm CADI. CADI stands for Contextual Anomaly Detection with Isolation-forest.
CADI is based on a revisited version of the Isolation Forest algorithm. CADI performs anomaly detection then clustering of regular instances. 
It then finds from which cluster each anomaly is deviating, and weights the features according to their contribution to making the anomaly closer to the cluster and making the anomaly deviating from the cluster.
The method is described in the following paper: Yepmo, V., Smits, G., Lesot, M. J., & Pivert, O. (2024, April). CADI: Contextual Anomaly Detection using an Isolation Forest. In The 39th ACM/SIGAPP Symposium On Applied Computing. 
The method is described in details in the following PhD thesis: Tchaghe, V. Y. (2023). Contribution to Anomaly Detection and Explanation (Doctoral dissertation, Université de Rennes).

# Using CADI

## Content of the project

This project contains two folders:

- [ ] Data: this folder contains the data sets
- [ ] Src: this folder contains the sources of the method. The files are:
  - dataset.py containing the Dataset class
  - forest.py containing the Forest class
  - node.py containing the Node class
  - tree.py containing the Tree class
  - utils.py containing some useful functions (mainly employed by the Forest class for clustering)
  - viewer.py containing some functions to produce some plots



## Requirements

CADI uses the following Python libraries:

- [ ] Numpy
- [ ] Scikit-learn (to compute AUROCs for anomaly detection evaluation)

## In practice

### Imports
```python 
from cadi.Src.forest import Forest
from cadi.Src.dataset import *
```
### Loading the dataset
```python
d = Dataset("data.csv", labels=False)
```

### Building the forest
```python
nbT = 100
f = Forest(d, nbT, "cadi", maxHeight=256)
f.build()
```

### Anomaly Detection
```python
f.anomalyDetection(binary=True, contamination_rate=0.05)
```

### Clustering
```python
f.clustering()
print(f.clusters_affectations) # to see the clusters affectations of each point (-1 for anomalies)
```

### Explanations Generation
```python
f.explain_anomalies()
print(f.explanations) # to see the explanations computed for each anomaly
```

# Authors and acknowledgment
For any inquiry, send me an e-mail (my mail adress can be found on the paper) :-)

Thanks to my co-authors :-)


